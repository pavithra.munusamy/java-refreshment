package org.test;

public class Stringcon2 {
	
	public static void main(String[] args) {
		
		String str="welcome";
		String str1="welcome";
		String str3=new String("welcome");
		String str4="Java";
		boolean a=str.equals(str1);	//true
		boolean b=str.equals(str3);	//true	
		boolean c=str.equals(str4);	//false
		boolean d=str1.equals(str3);//true
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);	//equals check the contents and not the reference
		//q.2palindorme		//(aabaa)
		//q2.occurences of letter in a particular word
		boolean a1=str==str1;	//true
		boolean a2=str==str3;	//false
		boolean a3=str==str4;	//false
		boolean a4=str==str3;	//false
		
		System.out.println(a1);
		System.out.println(a2);
		System.out.println(a3);
		System.out.println(a4);
		
	}

}
