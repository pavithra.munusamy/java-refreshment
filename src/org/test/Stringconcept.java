package org.test;

public class Stringconcept {
	public static void main(String[] args) {
		String s1="Welcome";	//stored in constant pool area
		char[] c= {'s','e','l','e','n','i','u','m'};
		String s2="welcome";	//string constant pool area
		String s3=new String("welcome");	//stored in heap memory
		String s4=new String("hello world");	//stored in heap memory
		String s5=new String(c);	//stored in heap memory
		System.out.println(s5);
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		System.out.println(s4);
		s1.concat("Java");
		System.out.println(s1);
		s1=s1.concat("Java");
		System.out.println(s1);
	}

}
