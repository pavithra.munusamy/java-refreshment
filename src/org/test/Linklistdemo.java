package org.test;

import java.util.LinkedList;

public class Linklistdemo {
	
	public static void main(String[] args) {
		LinkedList<String> list=new LinkedList<String>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		
		System.out.println(list);
		list.add(1,"a1");
		System.out.println(list);
		list.addFirst("x");
		list.addLast("z");
		System.out.println(list);
		list.remove("d");
		System.out.println(list);
	}

}

//a points b
//b points c
//c points d
//d points e
//add a1 after a

//a points a1
//a1 points b
//b points c
//c points d
//d points e