package org.test;

public class Sample {
	
	int a=0; //instance variable
	
	static int b=5; //static variable
	
	public void samp() {
		int c=20; //local variable
		
		a=a+5;
		System.out.println(a);
	}
	
	
	public static void main(String[] args) {
		
		Sample s=new Sample();
		Sample s1=new Sample();
	//	a=a+20; //we cannot call a instance without creating an object
	//	System.out.println(a);
		b=b-5;
		System.out.println(b); //we can access static variable without using an object
	//	c=c*2;	//the scope of local variable is only in the method
		
	}

}
